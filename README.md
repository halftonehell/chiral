# chiral

Perforce HelixCore Server Creation Tool

<div align="center">
    ![Screenshot, P4V login](readme/screenshot-chiral-p4v_login-01.png)
</div>


## About

This repository provides a quick creation tool for an on-premises Perforce HelixCore docker deployment. It is a proof-of-concept used for pipeline and tools development.

## Background

For development and testing of Unreal Engine plugins, I needed a quick and easy way to spin up/down multiple Perforce HelixCore servers with persistent storage. This repo is a tool to address that.

https://www.perforce.com/products/helix-core

## Requirements

- [Docker](https://www.docker.com)
- [Make](https://www.gnu.org/software/make/)

## Installation

_IMPORTANT_: This is an on-premises repo that uses self-signed certs for proof-of-concept. Do *NOT* put your real domain/production certs in git.

- Create a persistent storage directory for data and configuration.

```
❯ mkdir -p PerforceDemo/perforce-data PerforceDemo/p4dctl.conf.d
❯ cp chiral/docker/conf/main.conf PerforceDemo/p4dctl.conf.d/
```

- In *chiral/docker/dotenv*, set the desired bind root:

``` 
P4_BIND_ROOT=/Path/to/Persistent/Storage/PerforceDemo
```

- In *chiral/docker/tls*, replace the self-signed certs and _authorized_keys_ (for optional SSH) with your own.
- Build and start the container.

```
❯ cd chiral
❯ make -C docker build
❯ make -C docker up
```

- To stop the container:

```
❯ make -C docker down
```

You can now use Helix clients [P4V](https://www.perforce.com/downloads/helix-visual-client-p4v) and [P4](https://www.perforce.com/products/helix-core-apps/command-line-client) to connect.


## Defaults

| User      | Password  | Availability  |
| ---       | ---       | ---           |
| root      | d0ck3r    | shell         |
| perforce  | Ch4n63M3  | shell, p4d    |
| foo       | P455w0rd  | p4d           |


## TODO

- [ ] Add automatic multiple user creation
- [ ] Add on-prem MariaDB storage/query of certs, public-keys

## Support

This proof-of-concept is an alpha, thus currently unsupported.

## License

Licensed under GNU GPL v3. See [LICENSE](LICENSE).
