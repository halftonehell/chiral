Changelog
=========

0.2.1 (2023-05-24)
------------------
- ISSUE-011: Changed minor formatting. (1254e42) [Roderick Constance]
- ISSUE-011: Added readme bits. (f0ab863) [Roderick Constance]

0.2.0 (2023-05-24)
------------------
- Stage: Cleaned up for deployment. (15cadbc) [Roderick Constance]
- Stage: Added automated foo user deployment. (c3954c2) [Roderick Constance]
- Stage: Fixed typo. (2057f8c) [Roderick Constance]
- Stage: Added init workaround for perforce user. (4fae452) [Roderick Constance]
- Stage: Disabled HTTP/S ports. (854bb85) [Roderick Constance]
- Stage: Removed foo user for security/simplicity. (664533b) [Roderick Constance]
- Stage: Removed dbs from volume mount. (4c9f3e4) [Roderick Constance]
- Stage: Updated for p4dctl.conf.d post-build configure. (337d2d5) [Roderick Constance]
- ISSUE-010: Changed for PerforceDemo. (d201a8b) [Roderick Constance]
- ISSUE-009: Removed gpgcheck on perforce repo. (f32f62f) [Roderick Constance]
- ISSUE-009: Changed host ports. (9e75aa5) [Roderick Constance]
- ISSUE-007: Added Ansible TODO list. (dee8a93) [Roderick Constance]
- ISSUE-007: Added dot-p4ignore for UE4 config. (c4feddd) [Roderick Constance]
- ISSUE-007: Updated Dockerfile/Makefile for p4 re/build. (3899b78) [Roderick Constance]
- ISSUE-007: Added ansible TODO. (89dd9fe) [Roderick Constance]
- ISSUE-007: Added docker-compose workflow. (47e290e) [Roderick Constance]
- ISSUE-007: Added helixcore config bits. (34d2697) [Roderick Constance]
- ISSUE-007: Fixed basic server install, added Perforce bits. (0594cea) [Roderick Constance]
- ISSUE-007: Updated ansible bits for localhost, docker mac limitations. (6540734) [Roderick Constance]
- ISSUE-007: Added ansible roles. (5e61145) [Roderick Constance]
- ISSUE-003: Fixed Perforce PGP sha256sum. (e561439) [Roderick Constance]
- ISSUE-003: Disabled PGP key check. (f65979f) [Roderick Constance]
- ISSUE-003: Added more Dockerfile/Makefile bits. (e67d5e8) [Roderick Constance]
- ISSUE-003: Added Dockerfile. (03e67a9) [Roderick Constance]
- ISSUE-001: Added basic repo bits. (dd707c0) [Roderick Constance]
- Initial commit. (579dac2) [JustAddRobots]
