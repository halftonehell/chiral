#!/usr/bin/bash

USER="foo"

# Enable $USER
enable_user() {
	sudo -i -u perforce p4 users | grep "^$USER" > /dev/null
	if [[ $? -ne 0 ]]; then
		echo "Creating user $USER"
		cat /usr/local/tmp/p4_user_$USER.txt | sudo -i -u perforce p4 user -i -f
		echo "Setting user $USER password"
		sudo -i -u perforce p4 passwd -P P455w0rd $USER
	fi
}

sudo -i -u perforce p4dctl start main
sleep 3
enable_user
